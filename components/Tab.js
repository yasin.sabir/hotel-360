import React from 'react';
import {View, Text, TouchableOpacity, Image, Dimensions} from 'react-native';

import Images from '../images';

export default Tab = ({label, accessibilityState, onPress}) => {
  const focused = accessibilityState.selected;
  const icon = !focused ? Images.icons[label] : Images.icons[`${label}Focused`];
  const screenHeight = Dimensions.get('window').height;
  const Fcolor = !focused ? '#fff' : '#365a04';

  return (
    <TouchableOpacity
      onPress={onPress}
      style={{flex: 1, justifyContent: 'flex-end', alignItems: 'center'}}>
      <View style={{alignItems: 'center'}}>
        <Image
          source={icon}
          style={{height: screenHeight * 0.038, resizeMode: 'contain'}}
        />
        <Text
          style={{
            fontSize: screenHeight * 0.0125,
            color: Fcolor,
            paddingTop: 4,
          }}>
          {label}
        </Text>
      </View>
    </TouchableOpacity>
  );
};
