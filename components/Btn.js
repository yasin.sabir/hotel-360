import React from 'react';
import {StyleSheet} from 'react-native';
import {Button, Title, Text} from 'react-native-paper';

const Btn = (props) => {
  return(
    <Button
    style={[styles.btn , props.style]}
    color="#fff"
    labelStyle={{fontSize: 9, textAlign: 'center'}}>
    {props.label}
  </Button>
  );
};

export default Btn;

const styles = StyleSheet.create({
    btn: {
        justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 14,
    }
});
