import React from 'react';
import {TouchableHighlight, Text, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const ProBigTile = (props) => {
  var [isPress, setIsPress] = React.useState(false);
  const navigation = useNavigation();

  return (
    <LinearGradient
      start={{x: 0, y: 0}}
      end={{x: 1, y: 0}}
      colors={['#8bd12a', '#e57b0d']}
      style={{height: 140, borderRadius: 10, marginBottom: 15}}>
      <TouchableHighlight
        style={isPress ? styles.tilePressed : styles.tileNormal}
        activeOpacity={1}
        underlayColor="#fff"
        //onHideUnderlay={() => setIsPress(false)}
        //onShowUnderlay={() => setIsPress(true)}

        onPress={() => {
          setIsPress(true);
          setTimeout(() => {
            setIsPress(false);
            navigation.navigate('Promotion Business');
          }, 500);
        }}>
        <View style={{}}>
          <Text
            style={isPress ? styles.tileTextPressed : styles.tileTextNormal}>
            {props.title}
          </Text>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 11,
              paddingTop: 3,
              paddingLeft: 14,
              paddingRight: 14,
              color: '#606060',
            }}>
            {props.content}
          </Text>
        </View>
      </TouchableHighlight>
    </LinearGradient>
  );
};

export default ProBigTile;

const styles = StyleSheet.create({
  tileNormal: {
    height: 140,
    width: '100%',
    borderWidth: 2,
    borderColor: '#cac8c8',
    borderRadius: 10,
    marginRight: 10,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  tilePressed: {
    height: 140,
    width: '100%',
    borderWidth: 3,
    borderColor: 'transparent',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  tileTextNormal: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  tileTextPressed: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '700',
    color: '#e57c0b',
  },
});
