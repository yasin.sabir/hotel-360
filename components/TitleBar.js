import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Btn from '../components/Btn';

const TitleBar = (props) => {
  return (
    <View
      style={{flexDirection: 'row', paddingHorizontal: '5.55%', marginTop: 18}}>
      <Text style={{fontSize: 19, fontWeight: '700'}}>{props.title}</Text>
    </View>
  );
};

export default TitleBar;

const styles = StyleSheet.create({});
