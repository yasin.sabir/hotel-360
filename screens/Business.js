import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  Keyboard,
} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import BigTile from '../components/BigTile';
import {Button, Modal, Portal, Provider} from 'react-native-paper';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from 'react-native-table-component';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';

const screenWidth = Dimensions.get('window').width;

const TableHead = (props) => {
  const headings = props.headings;
  const Items = headings.map((heading) => (
    <Text style={{paddingTop: 8, fontWeight: '700', fontSize: 13}}>
      {heading}
    </Text>
  ));
  return Items;
};
const TableItem = (props) => {
  const Items = props.Items;
  const check = Items.map((Item) =>
    Item == 'Check' ? (
      <Icon
        name="check"
        size={12}
        color="#e57c0b"
        style={{paddingVertical: 12}}
      />
    ) : (
      <Icon
        name="close"
        size={12}
        color="#bfbfbf"
        style={{paddingVertical: 5}}
      />
    ),
  );
  return check;
};

const Business = () => {
  const [SView, setSView] = React.useState('50%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('28%');
  };

  const _keyboardDidHide = () => {
    setSView('50%');
  };

  var [tableHead, setTableHeads] = React.useState([
    'M',
    'T',
    'W',
    'T',
    'F',
    'S',
    'S',
  ]);
  var [tableTitle, setTableTitle] = React.useState([
    'Breakfast',
    'Lunch',
    'Dinner',
    'AllDay',
  ]);
  var [tableData, setTableData] = React.useState([
    ['Check', 'unCheck', 'Check', 'Check', 'unCheck', 'Check', 'unCheck'],
    ['unCheck', 'Check', 'unCheck', 'Check', 'Check', 'unCheck', 'Check'],
    ['Check', 'Check', 'unCheck', 'Check', 'unCheck', 'Check', 'Check'],
    ['Check', 'Check', 'Check', 'Check', 'Check', 'Check', 'Check'],
  ]);

  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);

  return (
    <Provider>
      <SafeAreaView style={styles.container}>
        <BackgroundLayout />
        <LogoBar title={`360 PASS PARTNERS`} />
        <TitleBar title={`BUSINESS / TOPIC NAME`} />
        <View
          style={{
            height: SView,
            marginTop: 20,
            //   paddingLeft: '5.55%',
            //   paddingRight: '5.55%',
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Image
              source={require('../images/Hotel360-assets/promotion-placeholder-2.png')}
              style={{
                height: 200,
                width: screenWidth * 0.89,
                marginTop: 15,
                resizeMode: 'cover',
                borderRadius: 10,
                marginHorizontal: '5.55%',
              }}
            />
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: '5.55%',
                marginTop: 10,
              }}>
              <Button
                style={{
                  backgroundColor: '#8bd12a',
                  height: 60,
                  width: '48.5%',
                  borderRadius: 10,
                  marginRight: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                labelStyle={{color: '#fff', textAlign: 'center', fontSize: 11}}
                onPress={() => navigation.navigate('Category')}>
                BOOK NOW
              </Button>
              <Button
                style={{
                  backgroundColor: '#e57c0b',
                  height: 60,
                  width: '48.5%',
                  borderRadius: 10,
                  marginLeft: 5,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                labelStyle={{color: '#fff', textAlign: 'center', fontSize: 11}}>
                REQUEST 360 PASS
              </Button>
            </View>
            <Text
              style={{
                marginTop: 10,
                marginHorizontal: '8%',
                textAlign: 'center',
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor.Lorem ipsum dolor sit amet
            </Text>
            <View
              style={{
                marginHorizontal: '5.55%',
                marginVertical: 20,
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <View
                style={{
                  borderColor: '#cac8c8',
                  borderWidth: 1,
                  borderRadius: 10,
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 70,
                      paddingTop: 8,
                      fontSize: 18,
                      color: '#e57c0b',
                      fontWeight: '700',
                    }}>
                    OPEN
                  </Text>
                  <TableHead headings={tableHead} />
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 70,
                      paddingVertical: 12,
                      fontWeight: '700',
                      fontSize: 13,
                    }}>
                    Breakfast
                  </Text>
                  <TableItem Items={tableData[0]} />
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#cac8c8',
                    marginHorizontal: 15,
                  }}></View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 70,
                      paddingVertical: 12,
                      fontWeight: '700',
                      fontSize: 13,
                    }}>
                    Lunch
                  </Text>
                  <TableItem Items={tableData[1]} />
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#cac8c8',
                    marginHorizontal: 15,
                  }}></View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 70,
                      paddingVertical: 12,
                      fontWeight: '700',
                      fontSize: 13,
                    }}>
                    Dinner
                  </Text>
                  <TableItem Items={tableData[2]} />
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    borderBottomColor: '#cac8c8',
                    marginHorizontal: 15,
                  }}></View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      width: 70,
                      paddingVertical: 12,
                      fontWeight: '700',
                      fontSize: 13,
                    }}>
                    AllDay
                  </Text>
                  <TableItem Items={tableData[3]} />
                </View>
              </View>
            </View>
            <View style={{height: 200}}>
              <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}></MapView>
            </View>

            <Portal>
              <Modal
                visible={visible}
                onDismiss={hideModal}
                contentContainerStyle={styles.containerStyle}>
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  style={{marginVertical: 20}}>
                  <View style={{flexDirection: 'row', width: '100%'}}>
                    <View style={{width: '48.5%', marginRight: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed"
                      />
                    </View>
                    <View style={{width: '48.5%', marginLeft: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
                      />
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '48.5%', marginRight: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
                      />
                    </View>
                    <View style={{width: '48.5%', marginLeft: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
                      />
                    </View>
                  </View>
                  <View style={{flexDirection: 'row'}}>
                    <View style={{width: '48.5%', marginRight: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
                      />
                    </View>
                    <View style={{width: '48.5%', marginLeft: 4.5}}>
                      <BigTile
                        title="PROMOTION & OFFER"
                        content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
                      />
                    </View>
                  </View>
                </ScrollView>
              </Modal>
            </Portal>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignContent: 'center',
              }}>
              <Button
                style={{
                  backgroundColor: '#e57c0b',
                  height: 60,
                  width: '48.5%',
                  borderRadius: 10,
                  marginTop: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                }}
                labelStyle={{color: '#fff', textAlign: 'center', fontSize: 11}}
                onPress={showModal}>
                SPECIAL OFFERS
              </Button>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    </Provider>
  );
};

export default Business;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  map: {
    ...StyleSheet.absoluteFill,
  },
  containerStyle: {
    backgroundColor: 'white',
    paddingHorizontal: '5.55%',
    marginHorizontal: '5.55%',
    height: '60%',
    borderRadius: 10,
  },
});
