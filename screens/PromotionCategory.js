import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Keyboard,
} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import BigTile from '../components/ProBigTile';

const Category = () => {
  const [SView, setSView] = React.useState('50%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('28%');
  };

  const _keyboardDidHide = () => {
    setSView('50%');
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackgroundLayout />
      <LogoBar title={`SELECTED HOTEL NAME`} />
      <TitleBar title={`PROMOTION CATEGORY TITLE`} />
      <View
        style={{
          height: SView,
          marginTop: 20,
          paddingLeft: '5.55%',
          paddingRight: '5.55%',
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <View style={{width: '48.5%', marginRight: 5}}>
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
            </View>
            <View style={{width: '48.5%', marginLeft: 5}}>
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
              <BigTile
                title="PROMOTION/OFFER NAME"
                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor"
              />
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Category;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
