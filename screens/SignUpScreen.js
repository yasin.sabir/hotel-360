import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Image,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {Button, Title, Text, TextInput} from 'react-native-paper';
import Icon_FA from 'react-native-vector-icons/FontAwesome';
import Icon_FA_5 from 'react-native-vector-icons/FontAwesome5';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export default SignUpScreen = ({navigation}) => {
  const [value, onChangeText] = React.useState('');
  const [value2, onChangeText2] = React.useState('');
  const [value3, onChangeText3] = React.useState('');
  const [value4, onChangeText4] = React.useState('');

  return (
    <SafeAreaView
      style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#fff',
      }}>
      <Image
        source={require('../images/Hotel360-assets/BG-SignIn.png')}
        style={styles.background_image}
      />
      <ScrollView style={{flex: 1}}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            minHeight: 550,
            zIndex: 2,

            width: '100%',
            marginTop: 20,
            //marginTop: -80,
          }}>
          <Image
            source={require('../images/Hotel360-assets/hotel-logo.png')}
            style={{resizeMode: 'contain', width: '30%'}}
          />
          <Title style={styles.heading}>Create Your Account</Title>
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText3(text)}
            value={value3}
            placeholder="FIRST NAME"
            placeholderTextColor="#838e9d"
            right={
              <TextInput.Icon
                name={() => <Icon_FA name={'user'} size={15} color="#8bd12a" />}
              />
            }
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText4(text)}
            value={value4}
            placeholder="LAST NAME"
            placeholderTextColor="#838e9d"
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText(text)}
            value={value}
            placeholder="EMAIL ADDRESS"
            placeholderTextColor="#838e9d"
            right={
              <TextInput.Icon
                name={() => (
                  <Icon_FA name={'envelope'} size={15} color="#8bd12a" />
                )}
              />
            }
          />
          <TextInput
            style={styles.input}
            onChangeText={(text) => onChangeText2(text)}
            value={value2}
            placeholder="PASSWORD"
            placeholderTextColor="#838e9d"
            right={
              <TextInput.Icon
                name={() => (
                  <Icon_FA_5 name={'eye'} size={15} color="#8bd12a" />
                )}
              />
            }
          />

          <Button style={styles.btn} color="#fff" contentStyle={{}}>
            SIGN UP
          </Button>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              marginTop: 10,
              marginBottom: 20,
            }}>
            <Text style={{fontSize: 12}}>Already Have an Account? </Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('SignInScreen')}>
              <Text
                style={{
                  color: '#8bd12a',
                  textDecorationLine: 'underline',
                  fontSize: 12,
                }}>
                Sign In
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  btn: {
    backgroundColor: '#e57c0b',
    color: '#fff',
    borderRadius: 10,
    width: '46%',
    height: '11%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },

  heading: {
    color: '#333333',
    fontSize: 24,
  },
  input: {
    width: '90%',
    height: '10%',
    // borderColor: '#f0f3f7',
    borderWidth: 0,
    textAlign: 'center',
    marginBottom: 6,
    marginTop: 6,
    backgroundColor: '#fff',
    fontWeight: 'bold',
    borderRadius: 8,
    fontSize: 13,
    color: '#838e9d',
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
    justifyContent: 'center',
    borderColor: 'red',
  },
  background_image: {
    height: screenHeight - statusBar,
    width: screenWidth,
    resizeMode: 'stretch',
    position: 'absolute',
    bottom: 0,
  },
});
