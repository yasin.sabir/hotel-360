import React from 'react';
import {View, Text, SafeAreaView, StyleSheet, Dimensions,StatusBar,Image} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';


const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const Experiences = () => {
  return(
    <SafeAreaView style={styles.container}>
        <BackgroundLayout/>
        {/* <LogoBar/> */}
    </SafeAreaView>
  );
};

export default Experiences;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});



