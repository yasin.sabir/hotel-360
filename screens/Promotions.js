import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  ScrollView,
} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import PromotionTile from '../components/PromotionTile';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const Promotions = () => {
  return (
    <SafeAreaView style={styles.container}>
      <BackgroundLayout />
      <LogoBar title={`SELECTED HOTEL NAME`} />
      <TitleBar title={`PROMOTIONS & OFFERS`} />
      <View style={{height: 90, marginTop: 20, paddingLeft: '5.55%'}}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}>
          {/* <TouchableOpacity onPress={() => setTimeout(() => {navigation.navigate('Category')},1000) }> */}
          <PromotionTile title="PROMOTION CATEGORY" />
          {/* </TouchableOpacity> */}
          <PromotionTile title="PROMOTION CATEGORY" />
          <PromotionTile title="PROMOTION CATEGORY" />
        </ScrollView>
      </View>
      <Image
        source={require('../images/Hotel360-assets/promotion-placeholder.png')}
        style={{
          height: 200,
          width: screenWidth * 0.89,
          marginTop: 15,
          resizeMode: 'cover',
          marginHorizontal: '5.55%',
          borderRadius: 10,
        }}
      />
      <Text
        style={{
          marginTop: 10,
          marginHorizontal: '8%',
          textAlign: 'center',
        }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor.Lorem ipsum dolor sit amet, Lorem ipsum dolor
      </Text>
    </SafeAreaView>
  );
};

export default Promotions;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
