import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  ScrollView,
  Keyboard,
} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import Tile from '../components/VoucherTile';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const MyCoupons = () => {
  const [SView, setSView] = React.useState('50%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('28%');
  };

  const _keyboardDidHide = () => {
    setSView('50%');
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackgroundLayout />
      <LogoBar />
      <TitleBar title="MY VOUCHERS & COUPONS" />
      <View
        style={{
          height: SView,
          marginTop: 20,
          paddingLeft: '5.55%',
          paddingRight: '5.55%',
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{flexDirection: 'row', width: '100%'}}>
            <View style={{width: '48.5%', marginRight: 5}}>
              <Tile title="360 DISCOUNT PASS" />
            </View>
            <View style={{width: '48.5%', marginLeft: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
          </View>
          <View style={{flexDirection: 'row', width: '100%', marginTop: 15}}>
            <View style={{width: '48.5%', marginRight: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
            <View style={{width: '48.5%', marginLeft: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
          </View>
          <View style={{flexDirection: 'row', width: '100%', marginTop: 15}}>
            <View style={{width: '48.5%', marginRight: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
            <View style={{width: '48.5%', marginLeft: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
          </View>
          <View style={{flexDirection: 'row', width: '100%', marginTop: 15}}>
            <View style={{width: '48.5%', marginRight: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
            <View style={{width: '48.5%', marginLeft: 5}}>
              <Tile title="VOUCHER OR COUPON NAME" />
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default MyCoupons;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
