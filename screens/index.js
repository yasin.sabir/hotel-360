import React from 'react';
import {Image, Dimensions} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Provider} from 'react-redux';
import store from '../store';
import {useSelector} from 'react-redux';

import Me from './Me';
import MyCoupons from './MyCoupons';
import Promotions from './Promotions';
import Restaurants from './Restaurants';
import Experiences from './Experiences';
import Category from './Category';
import Business from './Business';
import PromotionCategory from './PromotionCategory';
import PromotionBusiness from './PromotionBusiness';
import DiscountPass from './DiscountPass';
import PassBusiness from './PassBusiness';
import TabComponent from '../components/Tab';

import RootStackScreen from './RootStackScreen';

const MeStack = createStackNavigator();
const MyCouponsStack = createStackNavigator();
const PromotionsStack = createStackNavigator();
const RestaurantsStack = createStackNavigator();
const ExperiencesStack = createStackNavigator();

const Tab = createBottomTabNavigator();

const Index = () => {
  const screenHeight = Dimensions.get('window').height;
  const margbot = '1.8%';

  const isLogged = useSelector((state) => state.isLogged);

  return (
    <NavigationContainer>
      {!isLogged ? (
        <Tab.Navigator
          tabBarOptions={{
            style: {
              height: '13.5%',
              alignItems: 'flex-end',
              backgroundColor: 'transparent',
              borderTopWidth: 0,
              position: 'absolute',
              left: 20,
              right: 20,
              bottom: margbot,
              elevation: 0,
            },
          }}>
          <Tab.Screen
            name="Me"
            component={MeStackScreen}
            options={{
              tabBarButton: (props) => <TabComponent label="Me" {...props} />,
            }}
          />
          <Tab.Screen
            name="MyCoupons"
            component={MyCouponsStackScreen}
            options={{
              tabBarButton: (props) => (
                <TabComponent label="MyCoupons" {...props} />
              ),
            }}
          />
          <Tab.Screen
            name="Promotions"
            component={PromotionsStackScreen}
            options={{
              tabBarButton: (props) => (
                <TabComponent label="Promotions" {...props} />
              ),
            }}
          />
          <Tab.Screen
            name="Restaurants"
            component={RestaurantsStackScreen}
            options={{
              tabBarButton: (props) => (
                <TabComponent label="Restaurants" {...props} />
              ),
            }}
          />
          <Tab.Screen
            name="Experiences"
            component={ExperiencesStackScreen}
            options={{
              tabBarButton: (props) => (
                <TabComponent label="Experiences" {...props} />
              ),
            }}
          />
        </Tab.Navigator>
      ) : (
        <RootStackScreen />
      )}
    </NavigationContainer>
  );
};

export default Index;

const MeStackScreen = ({navigation}) => {
  return (
    <MeStack.Navigator headerMode="none">
      <MeStack.Screen name="Me" component={Me} />
    </MeStack.Navigator>
  );
};

const MyCouponsStackScreen = ({navigation}) => {
  return (
    <MyCouponsStack.Navigator headerMode="none">
      <MyCouponsStack.Screen name="My Coupons" component={MyCoupons} />
      <MyCouponsStack.Screen name="Discount Pass" component={DiscountPass} />
      <MyCouponsStack.Screen name="Pass Business" component={PassBusiness} />
    </MyCouponsStack.Navigator>
  );
};

const PromotionsStackScreen = ({navigation}) => {
  return (
    <PromotionsStack.Navigator headerMode="none">
      <PromotionsStack.Screen name="Promotions" component={Promotions} />
      <PromotionsStack.Screen
        name="Promotion Category"
        component={PromotionCategory}
      />
      <PromotionsStack.Screen
        name="Promotion Business"
        component={PromotionBusiness}
      />
    </PromotionsStack.Navigator>
  );
};

const RestaurantsStackScreen = ({navigation}) => {
  return (
    <RestaurantsStack.Navigator headerMode="none">
      <RestaurantsStack.Screen name="Restaurants" component={Restaurants} />
      <RestaurantsStack.Screen name="Category" component={Category} />
      <RestaurantsStack.Screen name="Business" component={Business} />
    </RestaurantsStack.Navigator>
  );
};

const ExperiencesStackScreen = ({navigation}) => {
  return (
    <ExperiencesStack.Navigator headerMode="none">
      <ExperiencesStack.Screen name="Experiences" component={Experiences} />
    </ExperiencesStack.Navigator>
  );
};
