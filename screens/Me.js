import React, {useEffect} from 'react';
import {
  View,
  Image,
  Dimensions,
  ImageBackground,
  StatusBar,
  StyleSheet,
  Platform,
  TextInput,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Keyboard,
} from 'react-native';
import {Button, Title, Text} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Icon5 from 'react-native-vector-icons/FontAwesome5';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const Me = () => {
  const [SView, setSView] = React.useState('69.3%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('50%');
  };

  const _keyboardDidHide = () => {
    setSView('69.3%');
  };

  return (
    <SafeAreaView style={styles.container}>
      <Image
        source={require('../images/Hotel360-assets/background-layout-1.png')}
        style={styles.background_image}
      />
      <View style={styles.logo_row}>
        <View style={styles.logo_row_col_1}>
          <Image
            source={require('../images/Hotel360-assets/hotel-logo.png')}
            style={styles.logo_css}
          />
        </View>
        <View style={styles.logo_row_col_2}>
          <View style={styles.logo_row_col_2_view_1}>
            <Text
              style={{
                fontWeight: '700',
                color: '#5a5a5a',
                textAlignVertical: 'center',
              }}>
              SELECT HOTEL NAME
            </Text>
          </View>
          <View style={styles.logo_row_col_2_view_2}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                justifyContent: 'flex-end',
                maxHeight: 30,
              }}>
              <Button
                style={{
                  backgroundColor: '#e57c0b',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 14,
                }}
                color="#fff"
                labelStyle={{fontSize: 9, textAlign: 'center'}}>
                HELP
              </Button>
              <Button
                style={{
                  backgroundColor: '#8bd12a',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 14,
                  marginLeft: '3%',
                }}
                color="#fff"
                labelStyle={{fontSize: 9, textAlign: 'center'}}>
                LOGOUT
              </Button>
            </View>
            <View
              style={{
                borderWidth: 1,
                padding: 6,
                borderRadius: 32,
                overflow: 'hidden',
                marginLeft: 5,
                borderColor: '#cac8c8',
              }}>
              <Icon name="bell" size={18} color="#5a5a5a" />
            </View>
          </View>
        </View>
      </View>
      <View style={{marginTop: 10, marginHorizontal: '5.55%', height: SView}}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.contentScroll_view_1}>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/Hotel360-assets/guest-directory-icon.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  GUEST{'\n'}DIRECTORY
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/Hotel360-assets/whats-on.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  WHAT'S ON
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_left}>
                <Image
                  source={require('../images/Hotel360-assets/coupons.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  MY VOUCHERS {'\n'}& COUPONS
                </Title>
              </TouchableOpacity>
            </View>
            <View style={styles.contentScroll_view_2}>
              <TouchableOpacity style={styles.touchableBox_right}>
                <Image
                  source={require('../images/Hotel360-assets/restaurants-eaters.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  RESTAURANTS{'\n'}& EATERIES
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_right}>
                <Image
                  source={require('../images/Hotel360-assets/experiences.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  EXPERIENCES
                </Title>
              </TouchableOpacity>
              <TouchableOpacity style={styles.touchableBox_right_last}>
                <Image
                  source={require('../images/Hotel360-assets/promotions.png')}
                  style={{height: '38.31%', resizeMode: 'contain'}}
                />
                <Title
                  style={{
                    fontSize: 14,
                    lineHeight: 14,
                    textAlign: 'center',
                    marginTop: '8.42%',
                  }}>
                  RECOMMENDED {'\n'} PROMOTIONS {'\n'} & OFFERS
                </Title>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },

  logo_row: {
    flexDirection: 'row',
  },

  logo_row_col_1: {
    width: '35%',
    marginTop: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },

  logo_row_col_2: {
    marginTop: 20,
    width: '65%',
  },

  logo_row_col_2_view_1: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: '5.55%',
  },

  logo_row_col_2_view_2: {
    alignSelf: 'flex-end',
    justifyContent: 'center',
    flexDirection: 'row',
    marginRight: '5.55%',
    marginTop: 12,
    height: 33,
  },

  logo_css: {
    width: screenWidth * 0.2555,
    height: screenHeight * 0.12,
    resizeMode: 'contain',
  },

  background_image: {
    height: screenHeight - statusBar,
    width: screenWidth,
    resizeMode: 'stretch',
    position: 'absolute',
    bottom: 0,
  },

  contentScrollView: {
    flex: 1,
    marginTop: 20,
    width: screenWidth,
    maxHeight: '69.3%',
    paddingLeft: '5.55%',
    paddingRight: '5.55%',
    //   borderWidth: 1,
    //   borderColor: '#f00'
  },

  contentScroll_view_1: {
    flex: 1,
    justifyContent: 'center',
    width: '50%',
    paddingRight: '4.67%',
    alignContent: 'flex-start',
    maxHeight: '100%',
  },

  contentScroll_view_1_row: {
    flex: 1,
    flexDirection: 'row',
    maxHeight: 30,
  },

  contentScroll_view_2: {
    justifyContent: 'center',
    width: '50%',
    height: '100%',
    paddingLeft: '2%',
    paddingRight: '2%',
    borderRadius: 10,
  },

  touchableBox_left: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: 145,
    backgroundColor: '#fff',
    marginTop: '15%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  touchableBox_right: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: 145,
    backgroundColor: '#fff',
    marginTop: '15%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  touchableBox_right_last: {
    borderWidth: 1,
    borderColor: '#cac8c8',
    borderRadius: 10,
    height: 145,
    backgroundColor: '#fff',
    marginTop: '15%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
});
export default Me;
