import React from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  ScrollView,
  Image,
  Pressable,
} from 'react-native';
import {Button} from 'react-native-paper';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import Tile from '../components/CategoryTile';
import {TouchableOpacity} from 'react-native-gesture-handler';

const screenWidth = Dimensions.get('window').width;

const Restaurants = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <BackgroundLayout />
      <LogoBar title={`SELECTED HOTEL NAME`} />
      <TitleBar title={`PARENT CATEGORY TITLE`} />
      <View style={{height: 90, marginTop: 20, paddingLeft: '5.55%'}}>
        <ScrollView
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          scrollEventThrottle={16}>
          {/* <TouchableOpacity onPress={() => setTimeout(() => {navigation.navigate('Category')},1000) }> */}
          <Tile title="CHILD-CATEGORY TITLE" />
          {/* </TouchableOpacity> */}
          <Tile title="CHILD-CATEGORY TITLE" />
          <Tile title="CHILD-CATEGORY TITLE" />
        </ScrollView>
      </View>
      <Image
        source={require('../images/Placeholder.png')}
        style={{
          height: 200,
          width: screenWidth * 0.89,
          marginTop: 15,
          resizeMode: 'cover',
          marginHorizontal: '5.55%',
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          paddingHorizontal: '5.55%',
          marginTop: 10,
        }}>
        <Button
          style={{
            backgroundColor: '#8bd12a',
            height: 60,
            width: '48.5%',
            borderRadius: 10,
            marginRight: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          labelStyle={{color: '#fff', textAlign: 'center', fontSize: 12}}
          onPress={() => navigation.navigate('Category')}>
          MY VOUCHERS & COUPONS
        </Button>
        <Button
          style={{
            backgroundColor: '#e57c0b',
            height: 60,
            width: '48.5%',
            borderRadius: 10,
            marginLeft: 5,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          labelStyle={{color: '#fff', textAlign: 'center', fontSize: 12}}>
          MOST POPULAR
        </Button>
      </View>
    </SafeAreaView>
  );
};

export default Restaurants;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tileNormal: {
    height: 90,
    width: 150,
    borderWidth: 2,
    borderColor: '#cac8c8',
    borderRadius: 10,
    marginRight: 10,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  tilePressed: {
    height: 90,
    width: 150,
    borderWidth: 3,
    borderColor: 'transparent',
    borderRadius: 10,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#fff',
  },
  tileTextNormal: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  tileTextPressed: {
    textAlign: 'center',
    fontSize: 14,
    fontWeight: '700',
    color: '#e57c0b',
  },
});
