import React, {useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Image,
  Dimensions,
  Keyboard,
} from 'react-native';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';
import BigTile from '../components/BigTile';
import {Button, Modal, Portal, Provider} from 'react-native-paper';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';

const screenWidth = Dimensions.get('window').width;

const Business = () => {
  const [SView, setSView] = React.useState('50%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('28%');
  };

  const _keyboardDidHide = () => {
    setSView('50%');
  };

  const [visible, setVisible] = React.useState(false);

  const showModal = () => setVisible(true);
  const hideModal = () => setVisible(false);

  return (
    <Provider>
      <SafeAreaView style={styles.container}>
        <BackgroundLayout />
        <LogoBar title={`SELECTED HOTEL NAME`} />
        <TitleBar title={`PROMO BUSINESS / TOPIC NAME`} />
        <View
          style={{
            height: SView,
            marginTop: 20,
            //   paddingLeft: '5.55%',
            //   paddingRight: '5.55%',
          }}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Image
              source={require('../images/Hotel360-assets/promotion-placeholder-2.png')}
              style={{
                height: 200,
                width: screenWidth * 0.89,
                marginTop: 15,
                resizeMode: 'cover',
                borderRadius: 10,
                marginHorizontal: '5.55%',
              }}
            />
            <View
              style={{
                flexDirection: 'row',
                paddingHorizontal: '5.55%',
                marginTop: 20,
              }}>
              <Button
                style={{
                  backgroundColor: '#8bd12a',
                  height: 60,
                  width: '46.5%',
                  borderRadius: 10,
                  marginRight: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                labelStyle={{color: '#fff', textAlign: 'center', fontSize: 11}}
                onPress={showModal}>
                BOOK NOW
              </Button>
              <Button
                style={{
                  backgroundColor: '#e57c0b',
                  height: 60,
                  width: '46.5%',
                  borderRadius: 10,
                  marginLeft: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                labelStyle={{color: '#fff', textAlign: 'center', fontSize: 11}}>
                GET YOUR COUPON
              </Button>
            </View>
            <Text
              style={{
                marginVertical: 20,
                marginHorizontal: '8%',
                textAlign: 'center',
              }}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor.Lorem ipsum dolor sit amet
            </Text>
            <View style={{height: 200}}>
              <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: 37.78825,
                  longitude: -122.4324,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121,
                }}></MapView>
            </View>

            <Portal>
              <Modal
                visible={visible}
                onDismiss={hideModal}
                contentContainerStyle={styles.containerStyle}>
                <ScrollView
                  showsVerticalScrollIndicator={false}
                  style={{marginVertical: 20}}>
                  <Button
                    style={{
                      backgroundColor: '#8bd12a',
                      height: 58,
                      //width: '48.5%',
                      borderRadius: 10,
                      marginBottom: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    labelStyle={{
                      color: '#fff',
                      textAlign: 'center',
                      fontSize: 10,
                    }}>
                    CALL NOW
                  </Button>
                  <Button
                    style={{
                      backgroundColor: '#e57c0b',
                      height: 58,
                      //width: '48.5%',
                      borderRadius: 10,
                      marginTop: 5,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    labelStyle={{
                      color: '#fff',
                      textAlign: 'center',
                      fontSize: 10,
                    }}>
                    BOOK ONLINE
                  </Button>
                </ScrollView>
              </Modal>
            </Portal>
          </ScrollView>
        </View>
      </SafeAreaView>
    </Provider>
  );
};

export default Business;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  map: {
    ...StyleSheet.absoluteFill,
  },
  containerStyle: {
    backgroundColor: 'white',
    paddingHorizontal: '5.55%',
    marginHorizontal: '18%',
    height: '23%',
    borderRadius: 10,
  },
});
