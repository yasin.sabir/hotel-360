import React, {useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Dimensions,
  StatusBar,
  Image,
  ScrollView,
  Keyboard,
} from 'react-native';
import {Button} from 'react-native-paper';
import BackgroundLayout from '../components/BackgroundLayout';
import LogoBar from '../components/LogoBar';
import TitleBar from '../components/TitleBar';

const screenHeight = Dimensions.get('window').height;
const screenWidth = Dimensions.get('window').width;
const statusBar = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const PassBusiness = () => {
  const [SView, setSView] = React.useState('50%');

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', _keyboardDidHide);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
      Keyboard.removeListener('keyboardDidHide', _keyboardDidHide);
    };
  }, []);

  const _keyboardDidShow = () => {
    setSView('28%');
  };

  const _keyboardDidHide = () => {
    setSView('50%');
  };

  return (
    <SafeAreaView style={styles.container}>
      <BackgroundLayout />
      <LogoBar
        title={`360
PASS`}
        color="#650d88"
        borderWidth={5}
        borderColor="#e78200"
      />
      <TitleBar title={`NAME OF PASS BUSINESS`} />
      <View
        style={{
          height: SView,
          marginTop: 20,
          paddingLeft: '5.55%',
          paddingRight: '5.55%',
        }}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text
            style={{
              textAlign: 'center',
              paddingHorizontal: 15,
              color: '#6b6b6b',
            }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et
          </Text>
          <View
            style={{
              borderTopColor: '#edf0f5',
              borderTopWidth: 1,
              marginVertical: 15,
            }}></View>
          <View style={{justifyContent: 'center', alignContent: 'center'}}>
            <Text style={{textAlign: 'center', fontWeight: '700'}}>
              NET AMOUNT PAYABLE
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: '700',
                color: '#8bd12a',
                fontSize: 55,
                marginVertical: 5,
              }}>
              $36.00
            </Text>
            <Text style={{textAlign: 'center', fontWeight: '700'}}>
              Show your 360 Pass to your Server
            </Text>
          </View>
          <View
            style={{
              borderTopColor: '#edf0f5',
              borderTopWidth: 1,
              marginVertical: 15,
            }}></View>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                width: '50%',
                borderRightColor: '#edf0f5',
                borderRightWidth: 1,
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: '700',
                  marginRight: 20,
                }}>
                INSERT THE FULL{'\n'}VALUE OF YOUR BILL
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: '700',
                  color: '#838e9d',
                  backgroundColor: '#f0f3f7',
                  marginRight: 20,
                  marginTop: 6,
                  borderRadius: 5,
                  fontSize: 30,
                }}>
                $45.00
              </Text>
            </View>
            <View style={{width: '50%'}}>
              <Text style={{textAlign: 'center', marginTop: 5}}>
                AVAILABLE{'\n'}DISCOUNT
              </Text>
              <Text
                style={{textAlign: 'center', fontWeight: '700', fontSize: 30}}>
                20%
              </Text>
            </View>
          </View>
          <View
            style={{
              borderTopColor: '#edf0f5',
              borderTopWidth: 1,
              marginVertical: 15,
            }}></View>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: '50%', paddingRight: 20}}>
              <Button
                style={{
                  backgroundColor: '#8bd12a',
                  height: 50,
                  justifyContent: 'center',
                  alignContent: 'center',
                  borderRadius: 8,
                }}
                labelStyle={{color: '#fff'}}>
                VERIFY
              </Button>
            </View>
            <View style={{width: '50%'}}>
              <Text style={{textAlign: 'center'}}>CONFIRMATION CODE</Text>
              <Text
                style={{
                  textAlign: 'center',
                  color: '#838e9d',
                  fontSize: 33,
                  fontWeight: '700',
                  lineHeight: 38,
                }}>
                3FY8H2
              </Text>
            </View>
          </View>
          <View style={{justifyContent: 'center', alignContent: 'center'}}>
            <Button
              style={{
                backgroundColor: '#e57c0b',
                height: 50,
                justifyContent: 'center',
                alignContent: 'center',
                borderRadius: 8,
                width: '50%',
                alignSelf: 'center',
                marginVertical: 20,
              }}
              labelStyle={{color: '#fff'}}>
              CLOSE
            </Button>
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default PassBusiness;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
