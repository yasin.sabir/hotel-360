/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
//import App from './App';
import React from 'react';
import App from './screens/index';
import {name as appName} from './app.json';

import {Provider} from 'react-redux';
import store from './store';

const Wrapper = () => {
    return(<Provider store={store}><App/></Provider>);
};

AppRegistry.registerComponent(appName, () => Wrapper);
